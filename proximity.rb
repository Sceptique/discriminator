require 'matrix'

class Vector

  # self is a point
  def proximity_with(other_points)
    other_points.map do |point|
      (self - point).magnitude
    end
  end

end

def get_proximities(points)
  Matrix.rows(
    points.map do |point|
      point.proximity_with(points)
    end
  )
end
